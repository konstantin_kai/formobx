import { makeObservable, observable } from 'mobx';
import { debounce } from './debounce';
import { FormValue } from './FormValue';

export type ValidateBehavior = 'immediately' | 'delayed' | null;

export interface FormArgs<V extends string> {
  values: Iterable<[V, FormValue]>;
  validateAfterChange?: Iterable<[V, ValidateBehavior]>;
}

export class Form<V extends string> {
  constructor(args: FormArgs<V>) {
    this._values = new Map(args.values);
    this._validateAfterChange = new Map(args.validateAfterChange ?? []);

    makeObservable(this, {
      submiting: observable,
    });
  }

  private _values: ReadonlyMap<V, FormValue>;
  private _validateAfterChange: ReadonlyMap<V, ValidateBehavior>;
  private _fields!: V[];

  submiting: boolean = false;

  private _debouncedValidate = debounce((field: V) => this.validateField(field), 600);

  get fields(): V[] {
    return this._fields ??= [...this._values.keys()];
  }

  get values(): ReadonlyMap<V, string> {
    return new Map(this._fields.map((field) => ([field, this.getFieldValue(field).value])));
  }

  get errors(): ReadonlyMap<V, string> {
    return new Map(
      this.fields.filter((field) => !this.getFieldValue(field).isValid)
        .map((field) => ([field, this.getFieldValue(field).error])),
    );
  }

  getFieldValue(field: V): FormValue {
    if (!this._values.has(field)) {
      throw new ReferenceError(`"${field}" does not exist`);
    }

    return this._values.get(field)!;
  }

  changeFieldValue(field: V, value: string): void {
    this.getFieldValue(field).changeValue(value);

    if (this._validateAfterChange.has(field)) {
      const behavior = this._validateAfterChange.get(field) ?? 'delayed';

      if (behavior === 'immediately') {
        this.validateField(field);
      } else {
        this._debouncedValidate(field);
      }
    }
  }

  resetFieldError(field: V): void {
    this.getFieldValue(field).setError('');
  }

  async validateField(field: V): Promise<void> {
    await this.getFieldValue(field).validate();
  }

  async submit(): Promise<void> {
    if (this.submiting) {
      return;
    }

    await Promise.all(this._fields.map((field) => this.validateField(field)));

    const isValid = this._fields.every((field) => this.getFieldValue(field).isValid);

    if (isValid) {
      return;
    }

    throw new FormInvalidError(this.errors);
  }
}

export class FormInvalidError<V extends string> extends Error {
  constructor(public readonly errors: ReadonlyMap<V, string>) {
    super();
    Object.setPrototypeOf(this, FormInvalidError.prototype);
  }
}
