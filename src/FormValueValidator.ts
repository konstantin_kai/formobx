export type ValidatorFn = (value: string) => Promise<string>;
export type ValidatorPredicat = () => boolean;

export class FormValueValidator {
  constructor(validator: ValidatorFn, predicat?: ValidatorPredicat) {
    this._validator = validator;
    this._predicat = predicat ?? null;
  }

  private _validator: ValidatorFn;
  private _predicat: ValidatorPredicat | null;

  async validate(value: string): Promise<string> {
    let result = '';

    if (this._predicat !== null) {
      if (!this._predicat()) {
        return result;
      }
    }

    try {
      result = await this._validator(value);
    } catch (error) {
      result = `${error}`;
    }

    return result;
  }
}
