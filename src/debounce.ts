interface Cancelable {
  clear(): void;
}

export function debounce<T extends (...args: any[]) => any>(
  func: T,
  wait?: number,
): T & Cancelable {
  let timeout: number;
  wait ??= 166;

  function debounced(...args: any[]) {
    const later = () => {
      func(...args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  }

  debounced.clear = () => {
    clearTimeout(timeout);
  };

  return debounced as T & Cancelable;
}
