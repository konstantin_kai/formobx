import { FormValueValidator, ValidatorPredicat } from './FormValueValidator';

export type HtmlFormValidityStateValue = keyof Omit<ValidityState, 'valid' | 'customError'>;

export type HtmlFormValidatorElementRef = {
  current: HTMLInputElement | HTMLTextAreaElement | null;
}

export interface HtmlFormValidatorExtraArgs {
  predicat?: ValidatorPredicat;
  useValidationAliasAsMessage?: boolean;
}

export class HtmlFormValidator extends FormValueValidator {
  constructor(
    ref: HtmlFormValidatorElementRef,
    args?: HtmlFormValidatorExtraArgs
  ) {
    super(
      async () => {
        const useValidationAliasAsMessage = args?.useValidationAliasAsMessage ?? false;

        if (ref.current !== null && !ref.current.validity.valid) {
          if (useValidationAliasAsMessage) {
            for (const key in ref.current.validity) {
              if (!['valid', 'customError'].includes(key)) {
                if (ref.current.validity[key as HtmlFormValidityStateValue]) {
                  return key;
                }
              }
            }
          }

          return ref.current.validationMessage;
        }

        return '';
      },
      args?.predicat,
    );
  }
}
