import { action, makeObservable, observable, runInAction } from 'mobx';
import { FormValueValidator } from './FormValueValidator';

export interface FormValueArgs {
  initial?: string;
  validators?: FormValueValidator[];
}

export class FormValue {
  constructor(args?: FormValueArgs) {
    this.value = args?.initial ?? '';
    this.validators = args?.validators ?? [];

    makeObservable(this, {
      value: observable,
      inprogress: observable,
      error: observable,
      isValid: observable,
      changeValue: action,
      setError: action,
    })
  }

  value: string;
  inprogress: boolean = false;
  error: string = '';
  isValid: boolean = true;

  readonly validators: readonly FormValueValidator[];

  changeValue(value: string): void {
    this.value = value;
  }

  setError(value: string): void {
    this.error = value;
  }

  async validate(): Promise<void> {
    runInAction(() => {
      this.inprogress = true;
    });

    for (let index = 0; index < this.validators.length; index++) {
      const validator = this.validators[index];
      const result = await validator.validate(this.value);

      if (result.length > 0) {
        runInAction(() => {
          this.isValid = false;
          this.error = result;
          this.inprogress = false;
        });
        return;
      }
    }

    runInAction(() => {
      this.isValid = true;
      this.inprogress = false;
      this.error = '';
    });
  }
}

